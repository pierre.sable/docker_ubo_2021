 TP Docker : Les commandes de base

**Exercice 1 : Hello from ubuntu**
Le but de ce premier exercice est de lancer des containers basés sur l'image ubuntu
1. Instancier un container basé sur ubuntu en lui fournissant la commande ```echo hello```

   ```bash
   $ docker container run ubuntu echo hello
   ```

2. Quelles sont les étapes effectuées par le docker daemon ?

    - Le démon vérifie si l'image ubuntu est dispo localement
    - Il la télécharge si besoin (pull)
    - Puis il instancie le conteneur et exécute la commande echo hello

3. Lancez un container basé sur ubuntu sans lui spécifier de commande. Qu’observez-vous ?

   - Le conteneur est instancié et lan /bin/bash puis s'éteind puisque plus de processus actif
---

**Exercice 2 : shell intéractif**
Le but de cet exercice est lancer des containers en mode intéractif +tty
1. Instancier un container basé sur ubuntu en mode interactif sans lui spécifier de commande

  ```bash
  $ docker container run -it ubuntu
  ```

2. Que s’est-il passé ?

  - Instanciation du conteneur avec prise de contrôle

3. Quelle est la commande par défaut d’un container basé sur ubuntu ?

  - /bin/bash


4. Naviguez dans le système de fichiers
5. Utilisez le gestionnaire de package apt pour ajouter un package
```
$ apt update
$ apt install -y vim
```
6. Sortez du conteneur
7. Détruire le conteneur

   ```bash
   $ docker container rm [ID/NOM]
   ```

9. Réinstancier un conteneur basé sur l'image ubuntu  en mode interactif sans lui spécifier de commande
    - La commande vim est-elle disponible ? Pourquoi ?

      - non, car le nouveau conteneur repart de l'image de référence (qui ne contient vim)

---

**Exercice 3 : foreground / background**
Le but de cet exercice est de créer des containers en foreground et en background
1. Lancez un container basé sur nginx sans lui spécifier de commande. Que constatez-vous ?

   ```bash
   $ docker container run nginx
   ```

   - Le conteneur applicatif est instancié, on voit le démarrage nginx et la log. Ne rend pas la main => on est en foreground

2. Arrêter le container avec CTRL-C
Le container est t-il toujours en cours d’exécution ?

  - Signal kill au processus, le conteneur se stoppe

Note: dans un autre terminal, vous pouvez utiliser la command docker ps que nous détaillerons dans l'une des
prochaines lectures), et qui permet de lister les containers qui tournent sur la machine.

3. Lancez un container en background, toujours  basé sur nginx sans lui spécifier de commande.

  ```bash
  $ docker container run -d nginx
  ```

Le container est t-il toujours en cours d’exécution ?

  - oui


---

**Exercice 4 : liste des containers**
Le but de cet exercice est de montrer les différentes options pour lister les containers du
système
1. Listez les containers en cours d’exécution. Est ce que tous les containers que vous avez créés sont listés ?

  ```bash
  $  docker container ls
  ```

  - On ne visualise que les containers UP

2. Utilisez l’option -a pour voir également les containers qui ont été stoppés
3. Utilisez l’option -q pour ne lister que les IDs des containers (en cours d’exécution ou
stoppés)

---

**Exercice 5 : exec dans un container**
Le but de cet exercice est de montrer comment lancer un processus dans un container
existant

1. Lancez un shell bash, en mode interactif, dans le container nginx encore actif

  ```
  $ docker container exec -it 46d8dadbf609 bash
  ```

2. Listez les processus du container (ps -ef)

   ```bash
   $ apt update
   $ apt install -y procps
   $ ps -ef
   ```

    - Qu'observez vous par rapport aux identifiants des processus ?

      - Un nouveau process est généré dans le conteneur. Donc on peut le quitter sans éteindre le conteneur 


3. IL est possible alors d'nalyser le contenu de l'image, la configuration nginx etc...

---


**Exercice 6 : publication de port**
Le but de cet exercice est de créer un container en exposant un port sur la machine hôte
1. Lancez un container basé sur nginx en détaché et publiez le port 80 du container sur le port 8080 de l’hôte

   ```bash
   $ docker container run -d -p 8080:80  nginx
   ```

2. Vérifiez depuis votre navigateur que la page par défaut de nginx est servie sur
http://ip_docker_hote:8080
3. Lancez un second container en publiant le même port
Qu’observez-vous ?

  - Attention conflit d'utilisation de port identique sur le docker hôte


---

**Exercice 7 : inspection d'un container Le but de cet exercice est l'inspection d’un container**
1. Lancez, en background, un nouveau container basé sur nginx:1.20 en publiant le port 80
du container sur le port 3000 de la machine host.
Notez l'identifiant du container retourné par la commande précédente.

   ```bash
   $ docker container run -d -p 3000:80 nginx:1.20
   ```

2. Inspectez le container en utilisant son identifiant (inspect)

    ```bash
    $ docker container inspect ID
    ```

3. En utilisant le format Go template, récupérez le nom et l’IP du container

   ```bash
   $ sudo docker container inspec {ID/NOM}```
   ```

4. Manipuler les Go template pour récupérer d'autres information

   ```bash
   $ sudo docker container inspect -f "{{ .NetworkSettings.IPAddress }}"
   $ sudo docker container inspect -f "{{ .Name }}"
   ```

**Exercice 8 : cleanup**
Le but de cet exercice est de stopper et de supprimer les containers existants
1. Listez tous les containers (actifs et inactifs)

  ```bash
  $ docker container ls -a
  ```

2. Stoppez tous les containers encore actifs en fournissant la liste des IDs à la commande
stop

   ```bash
   $ docker container stop $(docker coontainer ls -q)
   ```

3. Vérifiez qu’il n’y a plus de containers actifs

    ```bash
    $ docker container ls
    ```

4. Listez les containers arrêtés

   ```bash
   $ docker container ls -a
   ```

5. Supprimez tous les containers

   ```bash
   $ docker container prune
   ```

6. Mode bourrin : tout détruire en force

  ```bash
  $ docker container rm -f $(docker container ls -qa)
  ```