# Formation docker 

## Docker Engine

- Installation via dépôts officiels de la distri
    - Dépendant du cylcle de livraison de la distri (pas forcément à jour)

- Installation depuis les dépôts officiels DOCKER

    - Utilisation du script docker :

        ```bash
        $ curl -fsSL https://get.docker.com -o get-docker.sh
        $ sudo sh get-docker.sh
        $ sudo usermod -aG docker stagiaire
        $ su - stagiaire
        $ docker version
        ```

## CLient docker

 - Commande : ```docker```

 - Aide : ```--help``

 - Lister les ressources :

   ```bash
   $ docker image ls
   $ docker container ls
   $ docker volume ls
   $ docker network ls
   ```

## Choix image :

- HUB docker : registry publique

> https://hub.docker.com/_/ubuntu

- Test image ubuntu

    ```bash
    $ sudo docker container run ubuntu
    ```

- Instanciation nouveau conteneur en *surchargeant* la commande de base

    ```bash
    $ sudo docker container run ubuntu cat /etc/os-release
    ```

- Instanciation nouveau conteneur avec prise de controle (interactif + tty)

    ```bash
    sudo docker container run -it ubuntu bash


## Cycle de vie du conteneur

- Par défaut un conteneur s'instancie, exécute la commande prévue par l'image et s'arrête si plus de processus à l'intérieur
- Un conteneur basé sur image applicative (daemon), ou qui exécute une commande qui dure,  déclenche un processus actif et donc reste "up"
- Mode détaché : background (instanciation du conteneur en background) => approche production

- Commandes de troubleshooting :

    - logs
    - exec
    - inspect
    - start/stop/restart

- Un conteneur s'instancie à partir de l'image de base et ouvre une "layer" R/W lié à son cycle de vie. Celle-ci est détruite si le conteneur est détruit.

    - /!\ Perte de données, pas de persistance par défaut

## Publication de port

- Rendre accessible l'application depuis l'extérieur du docker hôte

> https://docs.docker.com/config/containers/container-networking/



## Persistence des données : volumes

> https://docs.docker.com/storage/volumes/

- Volumes
    - Concepts :
        - Présenter des données au conteneur (car non dispo dans l'image)
        - Sauvegarder des données produites par le conteneur (par l'appli)

> https://docs.docker.com/engine/reference/commandline/volume_create/

docker volume create --driver local --opt type=nfs4 --opt o=addr=192.168.244.123,rw --opt device=:/stagiaire testnfs


## Network

> https://docs.docker.com/network/

- Historique: lier des conteneurs par leur nom réseau :

    > https://docs.docker.com/network/links/

    > /!\ LEGACY

- Méthode best-practive 
    - réseau bridge de type user-defined

    > https://docs.docker.com/network/bridge/

    - Mise en place automatique de DNS entre les conteneurs d'un même réseau


## Industrialisation - Infra As Code d'un projet Docker

- docker-compose

    - https://docs.docker.com/compose/install/

    ```bash
    $ sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    $ sudo chmod +x /usr/local/bin/docker-compose
    $ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    ```


- Compose file : 

> https://docs.docker.com/compose/compose-file/

- Déclenchement et commandes

```bash
$ docker-compose up
$ docker-compose up -d
$ docker-compose ps
$ docker-compose top
$ docker-compose down
$ docker-compose --help
```


## Dockerfile

https://docs.docker.com/engine/reference/builder/

https://www.docker.com/blog/intro-guide-to-dockerfile-best-practices/