# Creation application micro-service docker

Le but de ce TP est de déployer une application micro-service composée de technologies nginx, php et mariadb grace à docker-compose

## Récupération du projet modèle

> Depuis le docker Hôte : 

  - Se positioner dans le répertoire partagé ```/home/stagiaire```

  - Cloner le dépôt GitHub : https://github.com/psable/TP_Appli_microservice.git

	```bash
    $ sudo apt install git
	$ git clone https://github.com/psable/TP_Appli_microservice.git
	```

## Analyse docker-compose

- Regarder le docker-compose

    - On constate que le block network et le block volume sont déjà déclarés. Il faudra les utiliser dans la déclaration des conteneurs (services)

## Déclaration du premier micro service dans le docker-compose

- Déclarer le micro service nginx :

    - nom : mynginx
    - network : mynetwork
    - image: nginx:1.20-alpine
    - variables d'environnement : 
        - PHP_HOST: myphp
        - PHP_PORT: 9000
    - port publié : 8085
    - volume type bind : "./conf:/etc/nginx/templates"

## Test lancement docker-compose

- déclencher le docker-compose

    ```bash
    $ cd TP_Appli_microservice
    $ docker-compose up -d
    $ docker-compose ps
    ```

## Déclaration du second microservice PHP

- Déclarer le micro service php :

    - nom: myphp
    - image: php:fpm-bullseye
    - networks: "mynetwork"
    - volumes de type bind : "./php/:/srv/http/"

## Déclenchement docker-compose

- On peut rappeler la commande ```docker-compose up -d```

- Valider le status

- Tester l'URL web


## Déclaration du troisième microservice MARIADB

- Déclarer le dernier micro service mariadb :

  - nom: mybdd
  - image: mariadb:10.7
  - networks: "mynetwork"
  - environment: MARIADB_ROOT_PASSWORD: roottoor
  - volume docker : mybdd:/var/lib/mysql

## Déclenchement docker-compose

- On peut rappeler la commande ```docker-compose up -d```

- Valider le status


## Connexion php vers mariadb

- Nous allons activer le code php pour test une connexion vers le micro-service mariadb

    - Enlever les commentaires dans le fichier de code index.php
    - Dans la déclaration du micro-service php, ajouter les variables d'environnement suivantes :
      -  BDD_HOST: "mybdd"
      -  BDD_USER: "root"
      -  BDD_PASS: 'roottoor'

- Déclencher le docker-compose : ```docker-compose up -d```

- Tester l'URL

  - On constate un pb PDO : il manque une extension php

- Solution temporaire/test : installer l'extansion directement dans le conteneur et le restarter

    ```bash
    $ docker container exec -it tp_appli_microservice_myphp_1 bash
    root@a9d9220f8ffb:/var/www/html# docker-php-ext-install pdo pdo_mysql
    $ exit
    $ docker container restart tp_appli_microservice_myphp_1
    $ docker container diff tp_appli_microservice_myphp_1
    ```
- Pour conserver le changement on peut faire un commit et ainsi générer une nouvelle image directement :

  ```bash
  $ docker container commit -a "Pierre" -m "Ajout extension pdo pdo_mysql" tp_appli_microservice_myphp_1 phpfpmpdo:8.1
  ```

- On peut archiver une image locale

   ```bash
   $ docker image save -o phpfpmpdo.tar phpfpmpdo:8.1
   ```

- On peut loader une image 

   ```bash
   $ docker image load archive.tar
   ```
   


    