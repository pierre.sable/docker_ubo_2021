# Reprise jours 2


# Découverte image mariadb


## 1. Rechercher l'image officielle mariadb

> https://hub.docker.com/_/mariadb

## 2. Instancier un conteneur 

    - Vérifier le status

    - Il faut préciser une variable d'environnement à l'instanciation du conteneur

    ```bash
    docker run --detach --name some-mariadb --env MARIADB_ROOT_PASSWORD=my-secret-pw  mariadb:latest
    ```

    - Penser au debug

    ```bash
    $ docker container logs some-mariadb
    ```

## 3. Données de la base

- Analyser les volumes sur votre docker hôte

    ```bash
    $ docker volume ls
    ```

    - /!\ Un volume anonyme est créé à chaque instanciation d'un conteneur basé sur l'image mariadb (prévu dans l'image). Mal maitrisé


- Pour correctement instancier le conteneur il faut préciser un volume


```bash
docker run --detach --name some-mariadb2 --env MARIADB_ROOT_PASSWORD=my-secret-pw  -v myvolbdd:/var/lib/mysql mariadb:latest
```





